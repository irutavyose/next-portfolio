import Link from "next/link";
import Layout from "../components/Layout"

const About = () => (
    <Layout title="About...">
        <Link href="/">
            <a>Go to the Home </a>
        </Link>
        <p>Javascript a programmer</p>
        <img src="/static/javascript-log.jpeg" alt="logo" height="200px" />
    </Layout>
)

export default About;