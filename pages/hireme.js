import Layout from "../components/Layout"

const Hireme = () => (

    <Layout title="Hire Me...">
        <p>
            You can hire Me at {}
            <a href="/mailto:nderagakuraaxcel@gmail.com">nderagakuraaxcel@gmail.com</a>
        </p>
        
    </Layout>
)

export default Hireme;